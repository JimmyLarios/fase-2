﻿using SGV.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGV
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //Boton de ingreso al sistema
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (tbUsuario.Text != "")
            {
                if (tbContraseña.Text != "")
                {
                    UserModel user = new UserModel();
                    var validLogin = user.LoginUser(tbUsuario.Text, tbContraseña.Text);
                    if (tbUsuario.Text == "admin" && tbContraseña.Text == "admin")
                    {
                        tbUsuario.Text = "";
                        tbContraseña.Text = "";
                        Response.Redirect("RegistrarEmpleado.aspx");
                    }
                    else
                    {
                        if (validLogin == true)
                        {
                            tbUsuario.Text = "";
                            tbContraseña.Text = "";
                            Response.Redirect("Inicio.aspx");
                        }
                        else
                        {
                            this.Page.Response.Write("<script language='JavaScript'>window.alert('" + "Credenciales Incorrectas" + "'); </script>");
                            tbUsuario.Text = "";
                            tbContraseña.Text = "";
                            tbUsuario.Focus();
                        }
                    }
                }
                else
                {
                    this.Page.Response.Write("<script language='JavaScript'>window.alert('" + "Ingrese su contraseña" + "'); </script>");
                }
            }
            else
            {
                this.Page.Response.Write("<script language='JavaScript'>window.alert('" + "Ingrese su usuario" + "'); </script>");
            }
        }
    }
}