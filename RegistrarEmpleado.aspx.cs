﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using SGV.Clases;

namespace SGV
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //BOTON PARA ANALIZAR EL XML EMPLEADOS
        protected void Button1_Click(object sender, EventArgs e)
        {
            string filename = fuEmpleado.FileName;
            string ruta = "D:\\" + filename;
            //analiza el XML
            string cadena = "";
            int nit = 0;
            string nombres = "";
            string apellidos = "";
            string nacimiento = "";
            string direccion = "";
            string telefono = "";
            string celular = "";
            string email = "";
            int codigo_puesto = 0;
            int codigo_jefe = -1;
            string pass = "";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            XmlNodeList lista = doc.GetElementsByTagName("NIT");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                nit = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("nombres");
            for (int i = 0; i < lista.Count; i++)
            {
                nombres = lista[i].InnerXml;
                nombres = nombres.Trim();
            }
            lista = doc.GetElementsByTagName("apellidos");
            for (int i = 0; i < lista.Count; i++)
            {
                apellidos = lista[i].InnerXml;
                apellidos.Trim();
            }
            lista = doc.GetElementsByTagName("nacimiento");
            for (int i = 0; i < lista.Count; i++)
            {
                nacimiento = lista[i].InnerXml;
                nacimiento.Trim();
            }
            lista = doc.GetElementsByTagName("direccion");
            for (int i = 0; i < lista.Count; i++)
            {
                direccion = lista[i].InnerXml;
                direccion.Trim();
            }
            lista = doc.GetElementsByTagName("telefono");
            for (int i = 0; i < lista.Count; i++)
            {
                telefono = lista[i].InnerXml;
                telefono.Trim();
            }
            lista = doc.GetElementsByTagName("celular");
            for (int i = 0; i < lista.Count; i++)
            {
                celular = lista[i].InnerXml;
                celular.Trim();
            }
            lista = doc.GetElementsByTagName("email");
            for (int i = 0; i < lista.Count; i++)
            {
                email = lista[i].InnerXml;
                email.Trim();
            }
            lista = doc.GetElementsByTagName("codigo_puesto");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                codigo_puesto = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("codigo_jefe");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                cadena = cadena.Trim();
                if (cadena != "")
                {
                    codigo_jefe = Int32.Parse(cadena.Trim());
                }
            }
            lista = doc.GetElementsByTagName("pass");
            for (int i = 0; i < lista.Count; i++)
            {
                pass = lista[i].InnerXml;
                pass.Trim();
            }
            //hace los registros en la base de datos
            UserModel user = new UserModel();
            if (codigo_jefe != -1)
            {
                user.registrarUsuario(nit, nombres, apellidos, nacimiento, direccion, telefono, celular, email, pass, codigo_jefe, codigo_puesto);
            }
            else
            {
                user.registrarUsuario(nit, nombres, apellidos, nacimiento, direccion, telefono, celular, email, pass, codigo_puesto);
            }
        }

        //boton para registrar un empleado en la base de datos
        protected void Registrar_Click(object sender, EventArgs e)
        {
            UserModel user = new UserModel();
            if (tbNitJefe.Text != "")
            {
                user.registrarUsuario(Int32.Parse(tbNit.Text), tbNombre.Text, tbApellido.Text, tbFechaNacimiento.Text, tbDireccion.Text, tbTelefonoDomicilio.Text, tbTelefonoCelular.Text, tbCorreoElectronico.Text, tbContraseña.Text, Int32.Parse(tbNitJefe.Text), Int32.Parse(tbTipoEmpleado.Text));
                //limpiamos campos
                tbNit.Text = "";
                tbNombre.Text = "";
                tbApellido.Text = "";
                tbFechaNacimiento.Text = "";
                tbDireccion.Text = "";
                tbTelefonoDomicilio.Text = "";
                tbTelefonoCelular.Text = "";
                tbCorreoElectronico.Text = "";
                tbContraseña.Text = "";
                tbNitJefe.Text = "";
                tbTipoEmpleado.Text = "";
            }
            else
            {
                user.registrarUsuario(Int32.Parse(tbNit.Text), tbNombre.Text, tbApellido.Text, tbFechaNacimiento.Text, tbDireccion.Text, tbTelefonoDomicilio.Text, tbTelefonoCelular.Text, tbCorreoElectronico.Text, tbContraseña.Text, Int32.Parse(tbTipoEmpleado.Text));
                //limpiamos campos
                tbNit.Text = "";
                tbNombre.Text = "";
                tbApellido.Text = "";
                tbFechaNacimiento.Text = "";
                tbDireccion.Text = "";
                tbTelefonoDomicilio.Text = "";
                tbTelefonoCelular.Text = "";
                tbCorreoElectronico.Text = "";
                tbContraseña.Text = "";
                tbNitJefe.Text = "";
                tbTipoEmpleado.Text = "";
            }
        }

        protected void Inicio_Click(object sender, EventArgs e)
        {
            Response.Redirect("Inicio.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("CargarXML.aspx");
        }
    }
}