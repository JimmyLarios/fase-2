﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace SGV.Clases
{
    public class UserDao : ConnectionToSql
    {
        public bool Login(string user, string pass)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM empleado WHERE nit=@user and contraseña=@pass";
                    command.Parameters.AddWithValue("@user", user);
                    command.Parameters.AddWithValue("@pass", pass);
                    command.CommandType = CommandType.Text;
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }


        //REGISTRAR EMPLEADO
        //sin jefe
        public void registrarEmpleado(int nit, string nombre, string apellido, string fechaNacimiento,
            string direccion, string telefonoDomicilio, string telefonoCelular, string correoElectronico, string contrasena, int fkTipoEmpleado)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "INSERT INTO empleado (nit, nombre, apellido, fechaNacimiento," +
                        "direccion, telefonoDomiciolio, telefonoCelular, correoElectronico, contraseña, fkTipoEmpleado)" +
                        "VALUES(@nit, @nombre, @apellido, @fechaNacimiento, @direccion, @telefonoDomicilio, @telefonoCelular, @correoElectronico, @contrasena, @fkTipoEmpleado)";
                    command.Parameters.AddWithValue("@nit", nit);
                    command.Parameters.AddWithValue("@nombre", nombre);
                    command.Parameters.AddWithValue("@apellido", apellido);
                    command.Parameters.AddWithValue("@fechaNacimiento", fechaNacimiento);
                    command.Parameters.AddWithValue("@direccion", direccion);
                    command.Parameters.AddWithValue("@telefonoDomicilio", telefonoDomicilio);
                    command.Parameters.AddWithValue("@telefonoCelular", telefonoCelular);
                    command.Parameters.AddWithValue("@correoElectronico", correoElectronico);
                    command.Parameters.AddWithValue("@contrasena", contrasena);
                    command.Parameters.AddWithValue("@fkTipoEmpleado", fkTipoEmpleado);
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    connection.Close();


                }


            }


        }
        //con jefe
        public void registrarEmpleado(int nit, string nombre, string apellido, string fechaNacimiento,
            string direccion, string telefonoDomicilio, string telefonoCelular, string correoElectronico, string contrasena, int fkJefe, int fKTipoEmpleado)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "INSERT INTO empleado (nit, nombre, apellido, fechaNacimiento," +
                        "direccion, telefonoDomiciolio, telefonoCelular, correoElectronico, contraseña, fkJefe, fkTipoEmpleado)" +
                        "VALUES(@nit, @nombre, @apellido, @fechaNacimiento, @direccion, @telefonoDomicilio, @telefonoCelular, @correoElectronico, @contrasena, @fkJefe, @fkTipoEmpleado)";
                    command.Parameters.AddWithValue("@nit", nit);
                    command.Parameters.AddWithValue("@nombre", nombre);
                    command.Parameters.AddWithValue("@apellido", apellido);
                    command.Parameters.AddWithValue("@fechaNacimiento", fechaNacimiento);
                    command.Parameters.AddWithValue("@direccion", direccion);
                    command.Parameters.AddWithValue("@telefonoDomicilio", telefonoDomicilio);
                    command.Parameters.AddWithValue("@telefonoCelular", telefonoCelular);
                    command.Parameters.AddWithValue("@correoElectronico", correoElectronico);
                    command.Parameters.AddWithValue("@contrasena", contrasena);
                    command.Parameters.AddWithValue("@fkJefe", fkJefe);
                    command.Parameters.AddWithValue("@fkTipoEmpleado", fKTipoEmpleado);
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    connection.Close();


                }


            }


        }


        //REGISTRAR PUESTO (TIPO DE EMPLEADO)
        public void registrarPuesto(int codigoTipoEmpleado, string tipo)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var comando = new SqlCommand())
                {
                    comando.Connection = connection;
                    comando.CommandText = "INSERT INTO tipoEmpleado VALUES(@codigoTipoEmpleado, @tipo)";
                    comando.Parameters.AddWithValue("@codigoTipoEmpleado", codigoTipoEmpleado);
                    comando.Parameters.AddWithValue("@tipo", tipo);
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        //REGISTRAR CATEGORIA DE PRODUCTO
        public void registrarCategoria(int codigoCategoria, string nombreCategoria)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var comando = new SqlCommand())
                {
                    comando.Connection = connection;
                    comando.CommandText = "INSERT INTO categoriaProducto VALUES(@codigoCategoria, @nombreCategoria)";
                    comando.Parameters.AddWithValue("@codigoCategoria", codigoCategoria);
                    comando.Parameters.AddWithValue("@nombreCategoria", nombreCategoria);
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        //REGISTRAR PRODUCTO
        //solo con codigo, nombre, codigo de la categoria
        public void registrarProducto(int codigoProducto, string nombre, string descripcion,int codigoCategoria)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var comando = new SqlCommand())
                {
                    comando.Connection = connection;
                    comando.CommandText = "INSERT INTO producto (codigoProducto, nombre, descripcion, codigoCategoria) VALUES(@codigoProducto, @nombre, @descripcion, @codigoCategoria)";
                    comando.Parameters.AddWithValue("@codigoProducto", codigoProducto);
                    comando.Parameters.AddWithValue("@nombre", nombre);
                    comando.Parameters.AddWithValue("@descripcion", descripcion);
                    comando.Parameters.AddWithValue("@codigoCategoria", codigoCategoria);
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        //REGISTRAR CIUDAD/MUNICIPIO
        public void registrarCiudad(int codigoMunicipio, string nombre)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var comando = new SqlCommand())
                {
                    comando.Connection = connection;
                    comando.CommandText = "INSERT INTO municipio VALUES(@codigoMunicipio, @nombre)";
                    comando.Parameters.AddWithValue("@codigoMunicipio", codigoMunicipio);
                    comando.Parameters.AddWithValue("@nombre", nombre);
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        //REGISTRA DEPARTAMENTO
        public void registrarDepartamento(int codigoDepartamento, string nombre, int codigoMunicipio)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var comando = new SqlCommand())
                {
                    comando.Connection = connection;
                    comando.CommandText = "INSERT INTO departamento VALUES(@codigoDepartamento, @nombre, @codigoMunicipio)";
                    comando.Parameters.AddWithValue("@codigoDepartamento", codigoDepartamento);
                    comando.Parameters.AddWithValue("@nombre", nombre);
                    comando.Parameters.AddWithValue("@codigoMunicipio", codigoMunicipio);
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        //REGISTRAR CLIENTE PERSONA
        public void registrarClientePersona(int nit, string nombre, string apellido, string fechaNacimiento, string direccion, string telefonoDomicilio, string telefonoCelular, string correoElectronico, int fkCodigoMunicipio, int fkCodigoDepartamento, double limiteCredito, int diasCredito, int fkCodigoLista)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var comando = new SqlCommand())
                {
                    comando.Connection = connection;
                    comando.CommandText = "INSERT INTO clientePersona VALUES(@nit, @nombre, @apellido, @fechaNacimiento, @direccion, @telefonoDomicilio, @" +
                        "telefonoCelular, @correoElectronico, @fkCodigoMunicipio, @fkCodigoDepartamento, @limiteCredito, @diasCredito, @fkCodigoLista)";
                    comando.Parameters.AddWithValue("@nit", nit);
                    comando.Parameters.AddWithValue("@nombre", nombre);
                    comando.Parameters.AddWithValue("@apellido", apellido);
                    comando.Parameters.AddWithValue("@fechaNacimiento", fechaNacimiento);
                    comando.Parameters.AddWithValue("@direccion", direccion);
                    comando.Parameters.AddWithValue("@telefonoDomicilio", telefonoDomicilio);
                    comando.Parameters.AddWithValue("@telefonoCelular", telefonoCelular);
                    comando.Parameters.AddWithValue("@correoElectronico", correoElectronico);
                    comando.Parameters.AddWithValue("@fkCodigoMunicipio", fkCodigoMunicipio);
                    comando.Parameters.AddWithValue("@fkCodigoDepartamento", fkCodigoDepartamento);
                    comando.Parameters.AddWithValue("@limiteCredito", limiteCredito);
                    comando.Parameters.AddWithValue("@diasCredito", diasCredito);
                    comando.Parameters.AddWithValue("@fkCodigoLista", fkCodigoLista);
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }



        //REGISTRAR LISTA DE PRECIOS
        public void registrarListaDePrecios(int codigoLista, string nombre, string fechaInicio, string fechaFin)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var comando = new SqlCommand())
                {
                    comando.Connection = connection;
                    comando.CommandText = "INSERT INTO listaPrecio VALUES(@codigoLista, @nombre, @fechaInicio, @fechaFin)";
                    comando.Parameters.AddWithValue("@codigoLista", codigoLista);
                    comando.Parameters.AddWithValue("@nombre", nombre);
                    comando.Parameters.AddWithValue("@fechaInicio", fechaInicio);
                    comando.Parameters.AddWithValue("@fechaFin", fechaFin);
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
        //registrar item en la tabla detalleListaPrecio
        public void registrarItemDetalleListaPrecio(int fkCodigoLista, int fkCodigoProducto, double precio)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var comando = new SqlCommand())
                {
                    comando.Connection = connection;
                    comando.CommandText = "INSERT INTO detalleListaPrecio VALUES(@fkCodigoLista, @fkCodigoProducto, @precio)";
                    comando.Parameters.AddWithValue("@fkCodigoLista", fkCodigoLista);
                    comando.Parameters.AddWithValue("@fkCodigoProducto", fkCodigoProducto);
                    comando.Parameters.AddWithValue("@precio", precio);
                    comando.CommandType = CommandType.Text;
                    comando.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }


        //obtener precio de producto
        public DataTable getPrecioProductos(int codigo)
        {
            DataSet ds = new DataSet();
            using (var conexion = GetConnection())
            {
                conexion.Open();
                SqlCommand comando = new SqlCommand("select precio from detalleListaPrecio INNER JOIN producto on" +
                    " detalleListaPrecio.fkCodigoProducto=" + codigo, conexion);
                SqlDataAdapter ad = new SqlDataAdapter(comando);
                ad.Fill(ds, "tabla");
                conexion.Close();
                return ds.Tables["tabla"];
            }
        }


        //Mostrar productos en un drop downlist
        public DataSet DSET(string sentencia)
        {
            DataSet ds = new DataSet();
            using (var connection = GetConnection())
            {
                connection.Open();
                try
                {
                    SqlDataAdapter SDA = new SqlDataAdapter(sentencia, connection);
                    SDA.Fill(ds, "datos");
                }
                catch (SqlException mise)
                {
                    int error = Convert.ToInt32(mise);
                }
            }
            return ds;
        }
        //MOSTRAR PRODUCTOS
        public void getDatosEnDropDownListProducto(string sentencia, DropDownList ddl)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                ddl.DataSource = DSET(sentencia);
                ddl.DataMember = "datos";
                ddl.DataTextField = "nombre";
                ddl.DataValueField = "codigoProducto";
                ddl.DataBind();
            }
        }
        //MOSTRAR CLIENTES
        
    }
}