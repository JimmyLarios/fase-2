﻿using Microsoft.Office.Tools.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace SGV.Clases
{
    public class Empleado
    {
        int nit;
        string nombre;
        string apellido;
        string fechaNacimineto;//posiblemente haya que cambiarlo por un tipo Date
        string direccion;
        string telefonoDomicilio;
        string telefonoCelular;
        string correoElectronico;
        int codigoTipoEmpleado; //llave foranea del tipo de empleado(CODIGO PUESTO)
        int codigoJefe; //llave foranea del jefe
        string contrasena;

        //constructor completo
        public Empleado(int nit, string nombre, string apellido, string fechaNacimiento,
            string direccion, string telefonoDomicilio, string telefonoCelular, int codigoTipoEmpleado
            , int codigoJefe, string contrasena)
        {
            this.nit = nit;
            this.nombre = nombre;
            this.apellido = apellido;
            this.fechaNacimineto = fechaNacimiento;
            this.direccion = direccion;
            this.telefonoDomicilio = telefonoDomicilio;
            this.telefonoCelular = telefonoCelular;
            this.codigoTipoEmpleado = codigoTipoEmpleado;
            this.codigoJefe = codigoJefe;
            this.contrasena = contrasena;
        }

        //constructor sin codigo del jefe
        public Empleado(int nit, string nombre, string apellido, string fechaNacimiento,
            string direccion, string telefonoDomicilio, string telefonoCelular, int codigoTipoEmpleado
            , string contrasena)
        {
            this.nit = nit;
            this.nombre = nombre;
            this.apellido = apellido;
            this.fechaNacimineto = fechaNacimiento;
            this.direccion = direccion;
            this.telefonoDomicilio = telefonoDomicilio;
            this.telefonoCelular = telefonoCelular;
            this.codigoTipoEmpleado = codigoTipoEmpleado;
            this.contrasena = contrasena;
        }


			
    }
}