﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace SGV.Clases
{
    public class UserModel
    {
        UserDao userDao = new UserDao();

        //INICIAR SESIÓN
        public bool LoginUser(string user, string pass)
        {
            return userDao.Login(user, pass);
        }

        //REGISTRAR USUARIO
        //sin llave foranea referente al jefe
        public void registrarUsuario(int nit, string nombre, string apellido, string fechaNacimiento, string direccion, string telefonoDomicilio, string telefonoCelular, string correoElectronico, string contrasena, int fkTipoEmpleado)
        {
            userDao.registrarEmpleado(nit, nombre, apellido, fechaNacimiento, direccion, telefonoDomicilio, telefonoCelular, correoElectronico, contrasena, fkTipoEmpleado);
        }
        //con llave foranea del jefe
        public void registrarUsuario(int nit, string nombre, string apellido, string fechaNacimiento, string direccion, string telefonoDomicilio, string telefonoCelular, string correoElectronico, string contrasena, int fkJefe, int fkTipoEmpleado)
        {
            userDao.registrarEmpleado(nit, nombre, apellido, fechaNacimiento, direccion, telefonoDomicilio, telefonoCelular, correoElectronico, contrasena, fkJefe, fkTipoEmpleado);
        }

        //REGISTRAR PUESTO (TIPO DE EMPLEADO)
        public void registrarPuesto(int codigoTipoEmpleado, string tipo)
        {
            userDao.registrarPuesto(codigoTipoEmpleado, tipo);
        }

        //REGISTRAR CATEGORIA DE PRODUCTO
        public void registrarCategoria(int codigoCategoria, string nombreCategoria)
        {
            userDao.registrarCategoria(codigoCategoria, nombreCategoria);
        }

        //REGISTRAR PRODUCTO solo con codigo, nombre y el codigo de la categoria
        public void registrarProducto(int codigo, string nombre, string descripcion,int categoria)
        {
            userDao.registrarProducto(codigo, nombre, descripcion,categoria);
        }

        //REGISTRAR MUNICIPIO
        public void registrarCiudad(int codigoMunicipio, string nombre)
        {
            userDao.registrarCiudad(codigoMunicipio, nombre);
        }


        //REGISTRAR DEPARTAMENTO
        public void registrarDepartamento(int codigoDepartamento, string nombre, int codigoMunicipio)
        {
            userDao.registrarDepartamento(codigoDepartamento, nombre, codigoMunicipio);
        }

        //REGISTRAR CLIENTE PERSONA
        public void registrarClientePersona(int nit, string nombre, string apellido, string fechaNacimiento, 
            string direccion, string telefonoDomicilio, string telefonoCelular, string correoElectronico, int fkCodigoMunicipio, 
            int fkCodigoDepartamento, double limiteCredito, int diasCredito, int fkCodigoLista)
        {
            userDao.registrarClientePersona(nit, nombre, apellido, fechaNacimiento,
                direccion, telefonoDomicilio, telefonoCelular, correoElectronico, fkCodigoMunicipio,
                fkCodigoDepartamento, limiteCredito, diasCredito, fkCodigoLista);
        }

        //REGISTRA LISTA DE PRECIOS
        public void registrarListaDePrecios(int codigoLista, string nombre, string fechaInicio, string fechaFin)
        {
            userDao.registrarListaDePrecios(codigoLista, nombre, fechaInicio, fechaFin);
        }
        //agregar cada item a la tabla detalleListaPrecio
        public void registrarItemDetalleListaPrecio(int fkCodigoLista, int fkCodigoProducto, double precio)
        {
            userDao.registrarItemDetalleListaPrecio(fkCodigoLista, fkCodigoProducto, precio);
        }



        //obtener precio de un producto
        public DataTable getPrecioProducto(int codigoProducto)
        {
            return userDao.getPrecioProductos(codigoProducto);
        }



        //MOSTRAR DATOS EN UN DROP DOWN LIST
        public void getDatosEnDropDownListProducto(string sentencia, DropDownList ddl) 
        {
            userDao.getDatosEnDropDownListProducto(sentencia, ddl);
        }


    }
}