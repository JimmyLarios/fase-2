﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGV.Clases
{
    public class Orden
    {
        Double total;
        string empleado;
        public Orden() 
        {
            total = 0;
        }
        

        //TOTAL
        public Double getTotal() 
        {
            return total;
        }
        public void sumarTotal(Double numero) 
        {
            this.total += numero;
        }
        public void restarTotal(Double numero)
        {
            this.total += numero;
        }

        //EMPLEADO
        public string getEmpleado()
        {
            return empleado;
        }
    }
}