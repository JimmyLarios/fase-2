﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="SGV.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2 style="text-align:center">Cerrar Orden</h2>
    <p style="text-align:center">&nbsp;</p>
    <p style="text-align:right">
        <asp:Label ID="Label1" runat="server" Font-Size="16pt" Text="Seleccione una orden pendiente de cierre: "></asp:Label>
        <asp:DropDownList ID="DropDownList1" runat="server" Font-Size="16pt" Height="30px" Width="248px">
        </asp:DropDownList>
    </p>
    <p>&nbsp;</p>
    <p>
        <asp:Label ID="Label4" runat="server" Font-Size="16pt" Text="Nombre del cliente"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label2" runat="server" Font-Size="16pt" Text="Total:   "></asp:Label>
        <asp:Label ID="lTotal" runat="server" Font-Size="16pt" Text="$. 0.00"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label5" runat="server" Font-Size="16pt" Text="Crédito disponible:   "></asp:Label>
        <asp:Label ID="lCreditoDisponible" runat="server" Font-Size="16pt" Text="$. 0.00"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label6" runat="server" Text="monto minímo"></asp:Label>
    </p>
    <p>&nbsp;</p>
    <p style="text-align:center">
        <asp:Button ID="lCerrarOrden" runat="server" Text="Cerrar Orden" Font-Size="16pt" />
    </p>
    </div>
</asp:Content>
