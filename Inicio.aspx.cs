﻿using SGV.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SGV
{
    public partial class _Default : Page
    {
        public Orden ordenActual = new Orden();
        UserModel user1 = new UserModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            UserModel user = new UserModel();
            string obtenerProductos = "SELECT * FROM producto";
            if (!IsPostBack)
            {
                user.getDatosEnDropDownListProducto(obtenerProductos, ddlProducto);
            }
        }

        //boton agregar
        protected void Button1_Click(object sender, EventArgs e)
        {
            int codigoProducto = Int32.Parse(this.ddlProducto.SelectedValue);
            Double precioActual = Double.Parse(user1.getPrecioProducto(codigoProducto).Rows[0][0].ToString());
            Double cantidad = Int32.Parse(tbCantidad.Text);
            Double subTotal = precioActual * cantidad;
            this.ordenActual.sumarTotal(subTotal);
            this.lTotal.Text = "$. " + this.ordenActual.getTotal().ToString();



        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            this.Label1.Text = this.ordenActual.getTotal().ToString();
        }
    }
}