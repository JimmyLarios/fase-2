﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="SGV.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <p style="text-align:center">
            <asp:Label ID="Label1" runat="server" Font-Size="21pt" Text="Aprobar Orden"></asp:Label>
        </p>
        <p style="text-align:right">
            Seleccione una orden pendiente de ser aprobada:&nbsp;&nbsp;
            <asp:DropDownList ID="ddlOrdenAprobar" runat="server" Width="223px">
            </asp:DropDownList>
        </p>
        <p style="text-align:right">
            &nbsp;</p>
        <p style="text-align:left">
            Nombre del cliente:&nbsp;&nbsp;
            <asp:Label ID="lNombreCliente" runat="server" Text="Nombre del cliente"></asp:Label>
        </p>
        <p style="text-align:left">
            Total:&nbsp;&nbsp;
            <asp:Label ID="lTotal" runat="server" Font-Size="16pt" Text="$. 0.00"></asp:Label>
        </p>
        <p></p>
        <p style="text-align:center">
            <asp:Button ID="bAprobarOrden" runat="server" Text="Aprobar Orden" Width="300px" />
        </p>
        <p style="text-align:center">
            &nbsp;</p>
        <hr>
        <p style="text-align:center">
            <asp:Label ID="Label2" runat="server" Font-Size="21pt" Text="Anular Orden"></asp:Label>
        </p>
        <p style="text-align:right">
            Selecione la orden que desea anular:&nbsp;&nbsp;
            <asp:DropDownList ID="ddlOrdenAnular" runat="server" Width="223px" OnSelectedIndexChanged="ddlAnular_SelectedIndexChanged">
            </asp:DropDownList>
        </p>
        <p style="text-align:center">
            &nbsp;</p>
        <p style="text-align:left">
            Nombre del cliente:&nbsp;&nbsp; <asp:Label ID="lNombreClienteAnular" runat="server" Text="Nombre del cliente"></asp:Label>
        </p>
        <p style="text-align:left">
            Total:&nbsp;&nbsp;&nbsp; <asp:Label ID="lTotalAnular" runat="server" Font-Size="16pt" Text="$. 0.00"></asp:Label>
        </p>
        <p style="text-align:center">
            &nbsp;</p>
        <p style="text-align:center">
            <asp:Button ID="bAnularOrden" runat="server" Text="Anular Orden" Width="300px" />
        </p>
    </div>
</asp:Content>
