﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="SGV._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <p Style="text-align:center">
            <asp:Label ID="Label1" runat="server" Text="Crear Orden" Font-Size="21pt" Font-Bold="True"></asp:Label>
        </p>
        <p Style="text-align:right">
            <asp:Label ID="lUsuario" runat="server" Text="Usuario que realiza el proceso"></asp:Label>
        </p>
        <p Style="text-align:right">
            &nbsp;</p>
        <p style="text-align:right">
            <asp:Label ID="Label3" runat="server" Text="Selecione un cliente:"></asp:Label>
            <asp:DropDownList ID="ddlClientes" runat="server" AutoPostBack="True" Width="185px">
            </asp:DropDownList>
        </p>
        <p style="text-align:right">
            Total:&nbsp;&nbsp;
            <asp:Label ID="lTotal" runat="server" Text="$. 0.00"></asp:Label>
        </p>
        <p style="text-align:right">
            Límite de crédito:&nbsp;&nbsp;
            <asp:Label ID="lLimiteCredito" runat="server" Text="$. 0.00"></asp:Label>
        </p>
        <p style="text-align:right">
            Órdenes con saldo vencido:&nbsp;&nbsp;
            <asp:Label ID="lOrdenesSaldoVencido" runat="server" Text="0"></asp:Label>
        </p>
        <p style="text-align:right">
            &nbsp;</p>
        <p style="text-align:center">
            Seleccione un producto:
            <asp:DropDownList ID="ddlProducto" runat="server" AutoPostBack="True" Width="176px">
            </asp:DropDownList>
        &nbsp;Cantidad:&nbsp;&nbsp;
            <asp:TextBox ID="tbCantidad" runat="server" Width="73px"></asp:TextBox>
&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" Text="Agregar" Width="100px" OnClick="Button1_Click" />
&nbsp;
            <asp:Button ID="Button2" runat="server" Text="Modificar" Width="100px" OnClick="Button2_Click" />
&nbsp;
            <asp:Button ID="Button3" runat="server" Text="Eliminar" Width="100px" />
        </p>
        <hr>
        <p style="text-align:center">
            &nbsp;</p>
        <p style="text-align:center">
            &nbsp;</p>
        <p style="text-align:center">
            &nbsp;</p>
        <p>
            &nbsp;</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <p>
                &nbsp;</p>
        </div>
    </div>

</asp:Content>
