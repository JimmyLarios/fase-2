﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CargarXML.aspx.cs" Inherits="SGV.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" style="text-align:center" enctype="multipart/form-data">
        <asp:Label ID="Label1" runat="server" Font-Size="28pt" Text="Cargar Archivo XML"></asp:Label>
        <div>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="14pt" Text="Puesto (Tipo de Empleado)"></asp:Label>
            <br />
            <br />
            <asp:FileUpload ID="fuPuesto" runat="server" Width="500px" />
            <br />
            <br />
            <asp:Button ID="bPuesto" runat="server" Text="Analizar XML" Width="500px" OnClick="bPuesto_Click" />
            <br />
            <br />
            <br />
            <asp:Label ID="label" runat="server" Font-Bold="True" Font-Size="14pt" Text="Categoría (Producto)"></asp:Label>
            <br />
            <br />
            <asp:FileUpload ID="fuCategoria" runat="server" Width="500px" />
            <br />
            <br />
            <asp:Button ID="bCategoria" runat="server" Text="Analizar XML" Width="500px" OnClick="bCategoria_Click" />
            <br />
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="14pt" Text="Producto"></asp:Label>
            <br />
            <br />
            <asp:FileUpload ID="fuProducto" runat="server" Width="500px" />
            <br />
            <br />
            <asp:Button ID="bProducto" runat="server" Text="Analizar XML" Width="500px" OnClick="bProducto_Click" />
            <br />
            <br />
            <br />
            <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="14pt" Text="Lista"></asp:Label>
            <br />
            <br />
            <asp:FileUpload ID="fuLista" runat="server" Width="500px" />
            <br />
            <br />
            <asp:Button ID="Button5" runat="server" Text="Analizar XML" Width="500px" OnClick="Button5_Click" />
            <br />
            <br />
            <br />
            <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Size="14pt" Text="Ciudad (Municipio) corregir foranea"></asp:Label>
            <br />
            <br />
            <asp:FileUpload ID="fuCiudad" runat="server" Width="500px" />
            <br />
            <br />
            <asp:Button ID="bCiudad" runat="server" Text="Analizar XML" Width="500px" OnClick="bCiudad_Click" />
            <br />
            <br />
            <br />
            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="14pt" Text="Departamento corregir foranea"></asp:Label>
            <br />
            <br />
            <asp:FileUpload ID="fuDepartamento" runat="server" Width="500px" />
            <br />
            <br />
            <asp:Button ID="bDepartamento" runat="server" Text="Analizar XML" Width="500px" OnClick="bDepartamento_Click" />
            <br />
            <br />
            <br />
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="14pt" Text="Cliente"></asp:Label>
            <br />
            <br />
            <asp:FileUpload ID="fuClientePersona" runat="server" Width="500px" />
            <br />
            <br />
            <asp:Button ID="bCliente" runat="server" Text="Analizar XML" Width="500px" OnClick="bCliente_Click" />
            <br />
            <br />
            <br />
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="14pt" Text="--------Meta"></asp:Label>
            <br />
            <br />
            <asp:FileUpload ID="FileUpload10" runat="server" Width="500px" />
            <br />
            <br />
            <asp:Button ID="Button9" runat="server" Text="Analizar XML" Width="500px" />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
