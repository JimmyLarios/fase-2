﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegistrarEmpleado.aspx.cs" Inherits="SGV.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Registrar </title>
    <style type="text/css">
        #TextArea1 {
            height: 163px;
            width: 1218px;
        }
        #mostrarXml {
            height: 165px;
            width: 1163px;
        }
        #Text1 {
            width: 243px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="text-align:center">
        <asp:Label ID="Label1" runat="server" Font-Size="28pt" Text="Empleado" Font-Bold="True"></asp:Label>
        <br />
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Nit:"></asp:Label>
        <br />
        <asp:TextBox ID="tbNit" runat="server" Width="300px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label4" runat="server" Text="Nombre:"></asp:Label>
        <br />
        <asp:TextBox ID="tbNombre" runat="server" Width="300px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label5" runat="server" Text="Apellido:"></asp:Label>
        <br />
        <asp:TextBox ID="tbApellido" runat="server" Width="300px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label6" runat="server" Text="Fecha de Nacimiento:"></asp:Label>
        <br />
        <asp:TextBox ID="tbFechaNacimiento" runat="server" Width="300px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label7" runat="server" Text="Dirección:"></asp:Label>
        <br />
        <asp:TextBox ID="tbDireccion" runat="server" Width="300px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label8" runat="server" Text="Teléfono Domicilio:"></asp:Label>
        <br />
        <asp:TextBox ID="tbTelefonoDomicilio" runat="server" Width="300px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label9" runat="server" Text="Teléfono Celular:"></asp:Label>
        <br />
        <asp:TextBox ID="tbTelefonoCelular" runat="server" Width="300px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label10" runat="server" Text="Correo Eléctronico:"></asp:Label>
        <br />
        <asp:TextBox ID="tbCorreoElectronico" runat="server" Width="300px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label11" runat="server" Text="Contraseña:"></asp:Label>
        <br />
        <asp:TextBox ID="tbContraseña" runat="server" Width="300px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label12" runat="server" Text="Nit Jefe:"></asp:Label>
        <br />
        <asp:TextBox ID="tbNitJefe" runat="server" Width="300px"></asp:TextBox>
        <br />
        <asp:Label ID="Label16" runat="server" Font-Size="10pt" Text="*Opcional"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label15" runat="server" Text="Tipo de Empleado:"></asp:Label>
        <br />
        <asp:TextBox ID="tbTipoEmpleado" runat="server" Width="300px"></asp:TextBox>
        <br />
        <asp:Label ID="Label17" runat="server" Font-Size="10pt" Text="*1 - Gerente, 2 - Supervisor, 3 - vendedor"></asp:Label>
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="Registrar" runat="server" OnClick="Registrar_Click" Text="Registrar" Width="300px" />
        <br />
        <br />
        <br />
        <div>
            <asp:Label ID="Label2" runat="server" Text="Cargar archivo XML de empleados" Font-Bold="True" Font-Size="14pt"></asp:Label>
            :<br />
            <br />
            <asp:FileUpload ID="fuEmpleado" runat="server" Width="259px" />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Analizar" Width="300px" />
            <br />
            <br />
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Cargar XML" Width="300px" />
            <br />
            <br />
            <asp:Button ID="Inicio" runat="server" OnClick="Inicio_Click" Text="Ir a Inicio" Width="300px" />
            <br />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
