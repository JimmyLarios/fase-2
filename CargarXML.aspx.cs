﻿using SGV.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace SGV
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //cargar XML Puesto
        protected void bPuesto_Click(object sender, EventArgs e)
        {
            string filename = fuPuesto.FileName;
            string ruta = "D:\\" + filename;
            string cadena = "";
            int codigo = -1;
            string nombre = "";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            XmlNodeList lista = doc.GetElementsByTagName("codigo");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                codigo = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("nombre");
            for (int i = 0; i < lista.Count; i++)
            {
                nombre = lista[i].InnerXml;
                nombre = nombre.Trim();
            }
            //hace los registros en la BD
            UserModel user = new UserModel();
            user.registrarPuesto(codigo, nombre);
        }

        //cargar XML CategoriaProducto
        protected void bCategoria_Click(object sender, EventArgs e)
        {
            string filename = fuCategoria.FileName;
            string ruta = "D:\\" + filename;
            string cadena = "";
            int codigo = -1;
            string nombre = "";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            XmlNodeList lista = doc.GetElementsByTagName("codigo");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                codigo = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("nombre");
            for (int i = 0; i < lista.Count; i++)
            {
                nombre = lista[i].InnerXml;
                nombre = nombre.Trim();
            }
            //hace los registros en la BD
            UserModel user = new UserModel();
            user.registrarCategoria(codigo, nombre);
        }

        //cargar xml productos
        protected void bProducto_Click(object sender, EventArgs e)
        {
            string filename = fuProducto.FileName;
            string ruta = "D:\\" + filename;
            string cadena = "";
            int codigo = -1;
            string nombre = "";
            string descripcion = "";
            int codigoCategoria = -1;
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            XmlNodeList lista = doc.GetElementsByTagName("codigo");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                codigo = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("nombre");
            for (int i = 0; i < lista.Count; i++)
            {
                nombre = lista[i].InnerXml;
                nombre = nombre.Trim();
            }
            lista = doc.GetElementsByTagName("descripcion");
            for (int i = 0; i < lista.Count; i++)
            {
                descripcion = lista[i].InnerXml;
                descripcion = descripcion.Trim();
            }
            lista = doc.GetElementsByTagName("categoria");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                codigoCategoria = Int32.Parse(cadena.Trim());
            }
            //hace los registros en la BD
            UserModel user = new UserModel();
            user.registrarProducto(codigo, nombre, descripcion,codigoCategoria);
        }














        //cargar XML ciudad/municipio
        protected void bCiudad_Click(object sender, EventArgs e)
        {
            string filename = fuCiudad.FileName;
            string ruta = "D:\\" + filename;
            string cadena = "";
            int codigo = -1;
            string nombre = "";
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            XmlNodeList lista = doc.GetElementsByTagName("codigo");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                codigo = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("nombre");
            for (int i = 0; i < lista.Count; i++)
            {
                nombre = lista[i].InnerXml;
                nombre = nombre.Trim();
            }
            //hace los registros en la BD
            UserModel user = new UserModel();
            user.registrarCiudad(codigo, nombre);
        }

        //cargar XML departamento
        protected void bDepartamento_Click(object sender, EventArgs e)
        {
            string filename = fuDepartamento.FileName;
            string ruta = "D:\\" + filename;
            string cadena = "";
            int codigoDepartamento = -1;
            string nombre = "";
            int codigoMunicipio = -1;
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            XmlNodeList lista = doc.GetElementsByTagName("codigo");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                codigoDepartamento = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("nombre");
            for (int i = 0; i < lista.Count; i++)
            {
                nombre = lista[i].InnerXml;
                nombre = nombre.Trim();
            }
            lista = doc.GetElementsByTagName("codigo_ciudad");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                codigoMunicipio = Int32.Parse(cadena.Trim());
            }
            //hace los registros en la BD
            UserModel user = new UserModel();
            user.registrarDepartamento(codigoDepartamento, nombre, codigoMunicipio);
        }

        //cargar XML cliente
        protected void bCliente_Click(object sender, EventArgs e)
        {
            string filename = fuClientePersona.FileName;
            string ruta = "D:\\" + filename;
            string cadena = "";
            int nit = -1;
            string nombre = "";
            string apellido = "";
            string fechaNacimiento = "";
            string direccion = "";
            string telefonoDomicilio = "";
            string telefonoCelular = "";
            string correoElectronico = "";
            int fkCodigoMunicipio = -1;
            int fkCodigoDepartamento = -1;
            double limiteCredito = -1;
            int diasCredito = -1;
            int fkCodigoLista = -1;
            XmlDocument doc = new XmlDocument();
            doc.Load(ruta);
            XmlNodeList lista = doc.GetElementsByTagName("NIT");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                nit = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("nombres");
            for (int i = 0; i < lista.Count; i++)
            {
                nombre = lista[i].InnerXml;
                nombre = nombre.Trim();
            }
            lista = doc.GetElementsByTagName("apellidos");
            for (int i = 0; i < lista.Count; i++)
            {
                apellido = lista[i].InnerXml;
                apellido = apellido.Trim();
            }
            lista = doc.GetElementsByTagName("nacimiento");
            for (int i = 0; i < lista.Count; i++)
            {
                fechaNacimiento = lista[i].InnerXml;
                fechaNacimiento = fechaNacimiento.Trim();
            }
            lista = doc.GetElementsByTagName("direccion");
            for (int i = 0; i < lista.Count; i++)
            {
                direccion = lista[i].InnerXml;
                direccion = direccion.Trim();
            }
            lista = doc.GetElementsByTagName("telefono");
            for (int i = 0; i < lista.Count; i++)
            {
                telefonoDomicilio = lista[i].InnerXml;
                telefonoDomicilio = telefonoDomicilio.Trim();
            }
            lista = doc.GetElementsByTagName("celular");
            for (int i = 0; i < lista.Count; i++)
            {
                telefonoCelular= lista[i].InnerXml;
                telefonoCelular = telefonoCelular.Trim();
            }
            lista = doc.GetElementsByTagName("email");
            for (int i = 0; i < lista.Count; i++)
            {
                correoElectronico = lista[i].InnerXml;
                correoElectronico = correoElectronico.Trim();
            }
            lista = doc.GetElementsByTagName("ciudad");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                fkCodigoMunicipio = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("depto");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                fkCodigoDepartamento = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("limite_credito");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                limiteCredito = Double.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("días_credito");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                diasCredito = Int32.Parse(cadena.Trim());
            }
            lista = doc.GetElementsByTagName("codigo_lista");
            for (int i = 0; i < lista.Count; i++)
            {
                cadena = lista[i].InnerXml;
                fkCodigoLista= Int32.Parse(cadena.Trim());
            }
            //hace los registros en la BD
            UserModel user = new UserModel();
            user.registrarClientePersona(nit, nombre,apellido, fechaNacimiento, direccion, telefonoDomicilio, telefonoCelular, 
                correoElectronico, fkCodigoMunicipio, fkCodigoDepartamento, limiteCredito, diasCredito, fkCodigoLista);
        }

        //cargar XML lista de precios 
        protected void Button5_Click(object sender, EventArgs e)
        {
            int[] codigosProductos = new int[100];//almacena los codigo de los producots
            double[] precios = new double[100]; //almacena los precios de los productos
            int indiceVector = 0;
            string cadena = "";
            int codigoLista = -1;
            string nombre = "";
            string fechaInicio = "";
            string fechaFin = "";
            //obtiene la ruta del xml
            string filename = fuLista.FileName;
            string ruta = "D:\\" + filename;
            XmlReader lector = XmlReader.Create(ruta);
            //lee el xml línea por línea
            int dirigidor = 0;
            while (lector.Read())
            {
                //ANALIZA CADA ITEM QUE ENCUENTRA
                //si encuentra codigo de producto
                if ((lector.NodeType == XmlNodeType.Element) && (lector.Name == "codigo_producto"))
                {
                    dirigidor = 1;
                }
                //analiza el valor de cada item
                else if ((lector.NodeType == XmlNodeType.Element) && (lector.Name == "valor"))
                {
                    dirigidor = 2;
                }
                else if ((lector.NodeType == XmlNodeType.Element) && (lector.Name == "codigo"))
                {
                    dirigidor = 3;
                }
                else if ((lector.NodeType == XmlNodeType.Element) && (lector.Name == "nombre"))
                {
                    dirigidor = 4;
                }
                else if ((lector.NodeType == XmlNodeType.Element) && (lector.Name == "fecha_inicio"))
                {
                    dirigidor = 5;
                }
                else if ((lector.NodeType == XmlNodeType.Element) && (lector.Name == "fecha_fin"))
                {
                    dirigidor = 6;
                }
                //obtiene el codigo del producto
                else if (dirigidor == 1 && (lector.NodeType == XmlNodeType.Text))
                {
                    cadena = lector.Value.Trim();
                    codigosProductos[indiceVector] = Int32.Parse(cadena);
                    dirigidor = 0;
                    indiceVector++;
                }
                //obtiene el valor del producto
                else if (dirigidor == 2 && (lector.NodeType == XmlNodeType.Text))
                {
                    cadena = lector.Value.Trim();
                    precios[indiceVector] = Double.Parse(cadena);
                    dirigidor = 0;
                    indiceVector++;
                }
                //obtiene el codigo
                else if (dirigidor == 3 && (lector.NodeType == XmlNodeType.Text))
                {
                    cadena = lector.Value;
                    codigoLista = Int32.Parse(cadena.Trim());
                    dirigidor = 0;
                }
                //obtiene el nombre 
                else if (dirigidor == 4 && (lector.NodeType == XmlNodeType.Text))
                {
                    nombre = lector.Value.Trim();
                    dirigidor = 0;
                }
                //obtiene la fecha de inicio
                else if (dirigidor == 5 && (lector.NodeType == XmlNodeType.Text))
                {
                    fechaInicio= lector.Value.Trim();
                    dirigidor = 0;
                }
                //obtiene la fecha fin
                else if (dirigidor == 6 && (lector.NodeType == XmlNodeType.Text))
                {
                    fechaFin = lector.Value.Trim();
                    dirigidor = 0;
                }
            }
            UserModel user = new UserModel();
            user.registrarListaDePrecios(codigoLista, nombre, fechaInicio, fechaFin);
            //agregamos los productos y precios a la tabla detalleListaPrecio
            int i = 0;
            while (i < indiceVector)
            {   
                user.registrarItemDetalleListaPrecio(codigoLista, codigosProductos[i], precios[i+1]);
                i+=2;
            }
        }

        
    }
}